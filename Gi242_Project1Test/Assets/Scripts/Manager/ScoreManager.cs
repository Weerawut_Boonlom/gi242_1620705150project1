﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI showScore;
        //[SerializeField] private TextMeshProUGUI FinalScoretext;

        private int scoreup = 0;
        //[SerializeField] private TextMeshProUGUI PlayerHP;
        
        

        private GameManager gameManager;

        private int playScore;
        //private int playerHP;

        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
            //SetHP(0);
        }

        public  void SetScore(int score)
        {
            scoreup += score;
            scoreText.text = $"Score : {scoreup.ToString()}";
            showScore.text = $"Score : {scoreup.ToString()}";
            playScore = score;
        }

        /*public void SetHP(int Hp)
        {
            PlayerHP.text = $"Hp : {Hp}";
            playerHP = Hp;
        }*/
        
        private void Awake()
        {
            Debug.Assert(scoreText != null, "ScoreText cannot null");
        }

        private void OnRestarted()
        {
            scoreText.text = $"Player Score : {playScore}";
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
            ShowScoretext();
        }

        private void ShowScoretext()
        {
            showScore.gameObject.SetActive(true);
        }
    }
}


